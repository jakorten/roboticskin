// http://arduino.cc/en/Serial/ParseInt

// RGB:1,2,3;

void getRGBFromSerial() {
  if (Serial.available() > 0) {
    String input = Serial.readString();
    Serial.println(input);
    if ((input.startsWith("paasei")) or (input.startsWith("easteregg")) or (input.startsWith("regenboog")) or (input.startsWith("REGENBOOG")) or (input.startsWith("rainbow")) or (input.startsWith("RAINBOW"))) {
      rainbow(10);
    }
    if (input.startsWith("RGB:")) {
      if ((input.endsWith(";\n")) or (input.endsWith(";\r\n")) or (input.endsWith(";"))) {
        Serial.println("OK...");
        int r = 0;
        int g = 0;
        int b = 0;
        int progress = 0; // counter for ?, R, G or B
        int v = 0; // tijdelijke waarde
        for (int i = 0; i < input.length(); i++) {
          if (i > 2) {
            if ((input[i] == ':') or (input[i] == ',') or (input[i] == ';')) {
              if (progress == 1) {
                r = v;

              }
              if (progress == 2) {
                g = v;

              }
              if (progress == 3) {
                b = v;

                Serial.print("Setting new value to R:");
                Serial.print(r);
                Serial.print(", G:");
                Serial.print(g);
                Serial.print(", B:");
                Serial.print(b);
                Serial.println("!");

                colorWipe(strip.Color(r, g, b), 50);

                return;

              }
              progress++;
              v = 0;
            } else {
              if (input[i] == ' ') {
                // ignore spaces...
              } else {
                if ((input[i] >= '0') and (input[i] <= '9')) {
                  v = v * 10 + input[i] - '0';
                } else {
                  Serial.println("Ongeldige invoer, verwacht: RGB:1,2,3;");

                }
              }
            }
          }
        }


      }
    }
  }
}
