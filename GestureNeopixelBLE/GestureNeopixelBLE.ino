/*

 Demo programma Neopixelring met Nano 33 BLE Sense
 Links / rechts 'swipen' verandert kleuren.
 Serial input laat kleur instellen (RGB:1,2,3;)

 Johan Korten, April 2020

*/

#include <ArduinoBLE.h>
#include <Adafruit_NeoPixel.h>

#define PIXEL_PIN    6  // Digital IO pin connected to the NeoPixels.

#define PIXEL_COUNT 12  // Number of NeoPixels

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)



int     mode     = 0;    // Currently-active animation mode, 0-9
boolean newState = false;

BLEService ledService("19B10000-E8F2-537E-3F6C-D104768A1214"); // create service

// create switch characteristic and allow remote device to read and write
BLEByteCharacteristic switchCharacteristicR("19B10001-E8F2-537E-3F6C-D104768A1214", BLERead | BLEWrite);
BLEByteCharacteristic switchCharacteristicG("19B10001-E8F2-537E-3F6C-D104768A1215", BLERead | BLEWrite);
BLEByteCharacteristic switchCharacteristicB("19B10001-E8F2-537E-3F6C-D104768A1216", BLERead | BLEWrite);


void setup() {
  setupPixels();
  initGestures();
  setupBLE();
}

void loop() {
  // put your main code here, to run repeatedly:
  processInput();
  
  updatePixels();
}

void processInput() {
  BLE.poll();
  detectGestures();
  BLE.poll();
  getRGBFromSerial(); // not elegant, but convenient...
}
