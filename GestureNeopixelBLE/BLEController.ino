int _r = 0;
int _g = 0;
int _b = 0;

void setupBLE() {

  // begin initialization
  if (!BLE.begin()) {
    Serial.println("starting BLE failed!");

    //while (1);
  }
  
  // set the local name peripheral advertises
  BLE.setLocalName("SkinColor");
  BLE.setDeviceName("SkinColor");
  // set the UUID for the service this peripheral advertises
  BLE.setAdvertisedService(ledService);

  // add the characteristic to the service
  ledService.addCharacteristic(switchCharacteristicR);
  ledService.addCharacteristic(switchCharacteristicG);
  ledService.addCharacteristic(switchCharacteristicB);

  // add service
  BLE.addService(ledService);

  // assign event handlers for connected, disconnected to peripheral
  BLE.setEventHandler(BLEConnected, blePeripheralConnectHandler);
  BLE.setEventHandler(BLEDisconnected, blePeripheralDisconnectHandler);

  // assign event handlers for characteristic
  switchCharacteristicR.setEventHandler(BLEWritten, switchCharacteristicWritten);
  // set an initial value for the characteristic
  switchCharacteristicR.setValue(0);

  // assign event handlers for characteristic
  switchCharacteristicG.setEventHandler(BLEWritten, switchCharacteristicWritten);
  // set an initial value for the characteristic
  switchCharacteristicG.setValue(0);

  // assign event handlers for characteristic
  switchCharacteristicB.setEventHandler(BLEWritten, switchCharacteristicWritten);
  // set an initial value for the characteristic
  switchCharacteristicB.setValue(0);

  // start advertising
  BLE.advertise();

  Serial.println(("Bluetooth device active, waiting for connections..."));
}

void blePeripheralConnectHandler(BLEDevice central) {
  // central connected event handler
  Serial.print("Connected event, central: ");
  Serial.println(central.address());
}

void blePeripheralDisconnectHandler(BLEDevice central) {
  // central disconnected event handler
  Serial.print("Disconnected event, central: ");
  Serial.println(central.address());
}

void switchCharacteristicWritten(BLEDevice central, BLECharacteristic characteristic) {
  // central wrote new value to characteristic, update LED
  Serial.println(switchCharacteristicR.value());
  Serial.print("Characteristic event, written: ");
  
  if (switchCharacteristicR.value() >= 0) {
    _r = switchCharacteristicR.value();
    Serial.print("Red = ");
    Serial.println(_r);
  }
  
  if (switchCharacteristicG.value() >= 0) {
    _g = switchCharacteristicG.value();
    Serial.print("Green = ");
    Serial.println(_g);
  }
  
  if (switchCharacteristicB.value() >= 0) {
    _b = switchCharacteristicB.value();
    Serial.print("Blue = ");
    Serial.println(_b);
  }

  colorWipe(strip.Color(_r, _g, _b), 50);
}
