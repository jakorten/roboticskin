/*

 Demo programma Neopixelring met Nano 33 BLE Sense
 Links / rechts 'swipen' verandert kleuren.
 Serial input laat kleur instellen (RGB:1,2,3;)

 Johan Korten, April 2020

*/

#include <ArduinoBLE.h>

int     mode     = 0;    // Currently-active animation mode, 0-9
boolean newState = false;


void setup() {
  setupPixels();
  initGestures();
}

void loop() {
  // put your main code here, to run repeatedly:
  detectGestures();
  updatePixels();
}
